type t;

@get external length: t => int = "length";

@send external key: (t, int) => Js.Nullable.t<string> = "key";

@send external getItem: (t, string) => Js.Nullable.t<string> = "getItem";

@send external setItem: (t, string, string) => unit = "setItem";

@send external removeItem: (t, string) => unit = "removeItem";

@send external clear: t => unit = "clear";


