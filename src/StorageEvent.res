type t;

/* TODO not bothering with the constructor but feel free to open a PR.*/

@get external key: t => Js.Nullable.t<string> = "key";

@get external newValue: t => Js.Nullable.t<string> = "newValue";

@get external oldValue: t => Js.Nullable.t<string> = "oldValue";

@get external storageArea: t => Storage.t = "storageArea";

@get external url: t => string = "url";
