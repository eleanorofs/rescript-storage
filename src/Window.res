@get external localStorage: Dom.window => Storage.t = "localStorage";

@get external sessionStorage: Dom.window => Storage.t = "sessionStorage";
